# projet_peinture

projet_peinture est un site qui va présenter des peintures avec la possibilité de laisser des commentaires.

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-composer
* nodejs et npm

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony):

#
symfony check:requirements
#

### Lancer l'environnement de développement

#
composer install
npm install
npm run build
docker-compose up -d
symfony serve -d
#

### Ajouter des données de tests
#
symfony console doctrine:fixtures:load
#

## Lancer des tests

#
php bin/phpunit --testdox
#

## Production

## Envoie des mails de Contacts

Les mails de prise de contact sont stockées en BDD, pour les envoyer au peintre par mail, il faut mettre en place un cron sur:
#
symfony console app:send-contact
#